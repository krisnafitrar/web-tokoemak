-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 20 Nov 2019 pada 01.29
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tokoemak`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(1) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(1, 'Krisna Fitra', 'krisnafitrar@gmail.com', 'bm1.jpg', '$2y$10$PVW4MxlCY30f.z9GubXQI.p4NYHntyDJ5oCg5bimkfiguGDUNp2cu', 2, 1, 1572863437);

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_access_menu`
--

CREATE TABLE `admin_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin_access_menu`
--

INSERT INTO `admin_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 2, 1),
(3, 3, 2),
(4, 2, 3),
(7, 2, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `menu`) VALUES
(1, 'Manager'),
(2, 'Administrator'),
(3, 'Menu'),
(4, 'Management');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_role`
--

CREATE TABLE `admin_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin_role`
--

INSERT INTO `admin_role` (`id`, `role`) VALUES
(1, 'Founder'),
(2, 'Manager'),
(3, 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_sub_menu`
--

CREATE TABLE `admin_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin_sub_menu`
--

INSERT INTO `admin_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, '1', 'Dashboard', 'manager', 'fas fa-fw fa-tachometer-alt', 1),
(2, '2', 'My Profile', 'user', 'fas fa-fw fa-user', 1),
(3, '3', 'Menu Management', 'menu', 'fas fa-fw fa-folder', 1),
(4, '3', 'Submenu Management', 'menu/submenu', 'fas fa-fw fa-folder-open', 1),
(5, '1', 'Role', 'manager/role', 'fas fa-fw fa-user-tie', 1),
(6, '2', 'Edit Profile', 'user/edit', 'fas fa-fw fa-user-edit', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `kode_banner` varchar(10) NOT NULL,
  `gambar` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(10) NOT NULL,
  `kode_kategori` varchar(10) NOT NULL,
  `nama` varchar(256) NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `terjual` int(11) NOT NULL,
  `persentase_penjualan` double NOT NULL,
  `gambar` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`kode_barang`, `kode_kategori`, `nama`, `harga`, `stok`, `terjual`, `persentase_penjualan`, `gambar`) VALUES
('B001', 'K001', 'Beras 1KG Pandan Wangi Organik', 27500, 50, 0, 0, 'https://id-test-11.slatic.net/p/03d62a1eed55d4edd6a330a58178e108.jpg_340x340q80.jpg_.webp'),
('B002', 'K001', 'Sania Beras Premium 5Kg', 62500, 50, 0, 0, 'https://assets.klikindomaret.com/products/20090009/20090009_thumb.jpg'),
('B003', 'K001', 'Larisst Beras Kepala Super 5Kg', 62500, 50, 0, 0, 'https://assets.klikindomaret.com/products/20080198/20080198_thumb.jpg'),
('B004', 'K001', 'Indomaret Ketan Hitam 500G', 15500, 50, 0, 0, 'https://assets.klikindomaret.com/products/20088561/20088561_1.jpg'),
('B005', 'K001', 'Indomaret Ketan Putih 500G', 13000, 50, 0, 0, 'https://assets.klikindomaret.com/products/20088559/20088559_thumb.jpg'),
('B006', 'K001', 'Segitiga Biru Tepung Terigu 1000G', 10300, 50, 0, 0, 'https://assets.klikindomaret.com/products/10005599/10005599_thumb.jpg'),
('B007', 'K001', 'Rose Tepung Beras 500G', 8800, 50, 0, 0, 'https://assets.klikindomaret.com/products/10000509/10000509_thumb.jpg'),
('B008', 'K001', 'Telur Ayam Negeri (Per Butir)', 12500, 100, 0, 0, 'https://assets.klikindomaret.com/products/20052542/20052542_thumb.jpg'),
('B009', 'K001', 'Telur Ayam Omega 3 (Per Butir)', 2400, 100, 0, 0, 'https://assets.klikindomaret.com/products/10003795/10003795_thumb.jpg'),
('B010', 'K001', 'Tropicana Slim Sweetener Honey 50X2.6g', 41500, 50, 0, 0, 'https://assets.klikindomaret.com/products/20098747/20098747_thumb.jpg'),
('B011', 'K001', 'Gulaku Gula Tebu (Putih) Premium 1000G', 12500, 50, 0, 0, 'https://assets.klikindomaret.com/products/10021010/10021010_thumb.jpg'),
('B012', 'K001', 'Diabetasol Zero Calorie Sweetener 50''S 50X1g', 34200, 50, 0, 0, 'https://assets.klikindomaret.com/products/20042356/20042356_thumb.jpg'),
('B013', 'K001', 'Kopi Kapal Api Merah 380gram', 22300, 50, 0, 0, 'https://assets.klikindomaret.com/products/20098227/20098227_thumb.jpg'),
('B014', 'K001', 'Bimoli Minyak Goreng 1000ML', 13000, 50, 0, 0, 'https://assets.klikindomaret.com/products/10012338/10012338_thumb.jpg'),
('B015', 'K001', 'Fortune Minyak Goreng 2000ML', 21000, 50, 0, 0, 'https://assets.klikindomaret.com/products/10000442/10000442_thumb.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `filter_barang`
--

CREATE TABLE `filter_barang` (
  `id` int(11) NOT NULL,
  `kode_kategori` varchar(10) NOT NULL,
  `nama` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `filter_barang`
--

INSERT INTO `filter_barang` (`id`, `kode_kategori`, `nama`) VALUES
(1, 'K001', 'Beras'),
(2, 'K001', 'Minyak'),
(4, 'K001', 'Gula'),
(5, 'K001', 'Kopi'),
(6, 'K001', 'Telur');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `kode_kategori` varchar(10) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `gambar` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`kode_kategori`, `nama`, `gambar`) VALUES
('K001', 'Kebutuhan Pokok', 'https://i.postimg.cc/KjJpp1CT/rice.png'),
('K002', 'Bumbu Dapur', 'https://i.postimg.cc/pTMSmbDm/garlic.png'),
('K003', 'Snack & Minuman', 'https://i.postimg.cc/rwb3LtsC/chips.png'),
('K004', 'Peralatan Rumah', 'https://i.postimg.cc/XYXmhgjY/broom.png'),
('K005', 'Alat Mandi', 'https://i.postimg.cc/gcD727r0/soap.png'),
('K006', 'Make Up', 'https://i.postimg.cc/0QGBmWfc/make-up.png'),
('K007', 'Obat-Obatan', 'https://i.postimg.cc/nrHw14SZ/ointment.png'),
('K008', 'Lainnya', 'https://i.postimg.cc/VNCTsLby/dashboard.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_banner`
--

CREATE TABLE `kategori_banner` (
  `kode_banner` varchar(10) NOT NULL,
  `nama` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_banner`
--

INSERT INTO `kategori_banner` (`kode_banner`, `nama`) VALUES
('BN01', 'Home'),
('BN02', 'Iklan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nomor_handphone` varchar(15) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_favorit`
--

CREATE TABLE `user_favorit` (
  `id` int(11) NOT NULL,
  `nomor_handphone` varchar(15) NOT NULL,
  `kode_barang` varchar(10) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_poin`
--

CREATE TABLE `user_poin` (
  `id` int(11) NOT NULL,
  `nomor_handphone` varchar(15) NOT NULL,
  `poin` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_access_menu`
--
ALTER TABLE `admin_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_sub_menu`
--
ALTER TABLE `admin_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `filter_barang`
--
ALTER TABLE `filter_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kode_kategori`);

--
-- Indexes for table `kategori_banner`
--
ALTER TABLE `kategori_banner`
  ADD PRIMARY KEY (`kode_banner`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_favorit`
--
ALTER TABLE `user_favorit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_poin`
--
ALTER TABLE `user_poin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin_access_menu`
--
ALTER TABLE `admin_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `admin_sub_menu`
--
ALTER TABLE `admin_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `filter_barang`
--
ALTER TABLE `filter_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_favorit`
--
ALTER TABLE `user_favorit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_poin`
--
ALTER TABLE `user_poin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
