-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 21 Nov 2019 pada 05.22
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simsekolah`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `idguru` int(255) NOT NULL,
  `namaguru` varchar(255) DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `notlp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `idkelas` int(255) NOT NULL,
  `namakelas` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`idkelas`, `namakelas`) VALUES
(1, 'Kelas 1'),
(2, 'Kelas 2'),
(3, 'Kelas 3'),
(4, 'Kelas 4'),
(5, 'Kelas 5'),
(6, 'Kelas 6');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelasajar`
--

CREATE TABLE `kelasajar` (
  `idkelasajar` int(11) NOT NULL,
  `idkelas` int(255) NOT NULL,
  `idtahun` varchar(5) NOT NULL,
  `namakelassiswa` varchar(255) DEFAULT NULL,
  `gurukelas` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `kelasajar`
--

INSERT INTO `kelasajar` (`idkelasajar`, `idkelas`, `idtahun`, `namakelassiswa`, `gurukelas`) VALUES
(1, 1, '20191', 'Kelas 1 20191', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelasjurusan`
--

CREATE TABLE `kelasjurusan` (
  `id` int(11) NOT NULL,
  `namajurusan` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelasjurusan`
--

INSERT INTO `kelasjurusan` (`id`, `namajurusan`) VALUES
(1, 'IPA'),
(2, 'IPS');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelassiswa`
--

CREATE TABLE `kelassiswa` (
  `idkelasajar` int(255) NOT NULL,
  `nis` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `kelassiswa`
--

INSERT INTO `kelassiswa` (`idkelasajar`, `nis`) VALUES
(1, '10001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggaran`
--

CREATE TABLE `pelanggaran` (
  `idjenispelanggaran` int(255) NOT NULL,
  `namapelanggaran` varchar(255) DEFAULT NULL,
  `skor` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggaransiswa`
--

CREATE TABLE `pelanggaransiswa` (
  `idpelanggaran` int(255) NOT NULL,
  `nis` varchar(255) DEFAULT NULL,
  `idtahun` varchar(5) DEFAULT NULL,
  `tgl_pelanggaran` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `nis` varchar(255) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `notlp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`nis`, `nama`, `alamat`, `notlp`) VALUES
('10001', 'Ziya', 'Sini', '0897566');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahunakademik`
--

CREATE TABLE `tahunakademik` (
  `idtahun` varchar(5) NOT NULL,
  `namatahun` varchar(255) DEFAULT NULL,
  `isaktif` varchar(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `tahunakademik`
--

INSERT INTO `tahunakademik` (`idtahun`, `namatahun`, `isaktif`) VALUES
('20191', '2019/2020 Gasal', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(1) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(1, 'Krisna Fitra', 'krisnafitrar@gmail.com', 'bm1.jpg', '$2y$10$PVW4MxlCY30f.z9GubXQI.p4NYHntyDJ5oCg5bimkfiguGDUNp2cu', 2, 1, 1572863437),
(2, 'Krisna', 'krisnafffr@gmail.com', 'IMG_20181110_063408_087.jpg', '$2y$10$DIW6AyQvIhMx6QV9qdWP5OnASSWrmCqMkgaG0HsKGhp3e1Toj1gLS', 2, 1, 1574224996);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 2, 1),
(3, 3, 2),
(4, 2, 3),
(7, 2, 2),
(8, 2, 5),
(9, 2, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Master'),
(2, 'Profile'),
(3, 'Menu'),
(5, 'Manajemen Sekolah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Headmaster'),
(2, 'Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, '1', 'Dashboard', 'manager', 'fas fa-fw fa-tachometer-alt', 1),
(2, '2', 'My Profile', 'user', 'fas fa-fw fa-user', 1),
(3, '3', 'Menu Management', 'menu', 'fas fa-fw fa-folder', 1),
(4, '3', 'Submenu Management', 'menu/submenu', 'fas fa-fw fa-folder-open', 1),
(5, '1', 'Role', 'manager/role', 'fas fa-fw fa-user-tie', 1),
(6, '2', 'Edit Profile', 'user/edit', 'fas fa-fw fa-user-edit', 1),
(7, '5', 'Data Siswa', 'manajemen', 'fas fa-fw fa-user-graduate', 1),
(8, '5', 'Data Guru', 'manajemen/guru', 'fas fa-fw fa-chalkboard-teacher', 1),
(9, '5', 'Tahun Akademik', 'manajemen/tahun', 'fas fa-fw fa-calendar-alt', 1),
(10, '5', 'Pelanggaran', 'manajemen/pelanggaran', 'fas fa-fw fa-exclamation-circle', 1),
(11, '5', 'Pelanggaran Siswa', 'manajemen/pelanggaransiswa', 'fas fa-fw fa-user-ninja', 1),
(12, '5', 'Data Kelas', 'manajemen/kelas', 'fas fa-fw fa-chalkboard', 1),
(13, '5', 'Kelas Jurusan', 'manajemen/kelasjurusan', 'fas fa-fw fa-graduation-cap', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`idguru`) USING BTREE;

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`idkelas`) USING BTREE;

--
-- Indexes for table `kelasajar`
--
ALTER TABLE `kelasajar`
  ADD PRIMARY KEY (`idkelasajar`) USING BTREE,
  ADD KEY `fk_kelassiswa_tahun` (`idtahun`) USING BTREE,
  ADD KEY `fk_kelassiswa_guru` (`gurukelas`) USING BTREE,
  ADD KEY `idkelas` (`idkelas`) USING BTREE;

--
-- Indexes for table `kelasjurusan`
--
ALTER TABLE `kelasjurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelassiswa`
--
ALTER TABLE `kelassiswa`
  ADD KEY `fk_kelassiswa_ajar` (`idkelasajar`) USING BTREE,
  ADD KEY `fk_kelassiswa_siswa` (`nis`) USING BTREE;

--
-- Indexes for table `pelanggaran`
--
ALTER TABLE `pelanggaran`
  ADD PRIMARY KEY (`idjenispelanggaran`) USING BTREE;

--
-- Indexes for table `pelanggaransiswa`
--
ALTER TABLE `pelanggaransiswa`
  ADD PRIMARY KEY (`idpelanggaran`) USING BTREE,
  ADD KEY `fk_pelanggaransiswa_siswa` (`nis`) USING BTREE,
  ADD KEY `fk_pelanggaransiswa_tahun` (`idtahun`) USING BTREE;

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`) USING BTREE;

--
-- Indexes for table `tahunakademik`
--
ALTER TABLE `tahunakademik`
  ADD PRIMARY KEY (`idtahun`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `idguru` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kelasajar`
--
ALTER TABLE `kelasajar`
  MODIFY `idkelasajar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kelasjurusan`
--
ALTER TABLE `kelasjurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pelanggaran`
--
ALTER TABLE `pelanggaran`
  MODIFY `idjenispelanggaran` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pelanggaransiswa`
--
ALTER TABLE `pelanggaransiswa`
  MODIFY `idpelanggaran` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kelasajar`
--
ALTER TABLE `kelasajar`
  ADD CONSTRAINT `fk_kelassiswa_guru` FOREIGN KEY (`gurukelas`) REFERENCES `guru` (`idguru`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kelassiswa_kelas` FOREIGN KEY (`idkelas`) REFERENCES `kelas` (`idkelas`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kelassiswa_tahun` FOREIGN KEY (`idtahun`) REFERENCES `tahunakademik` (`idtahun`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelassiswa`
--
ALTER TABLE `kelassiswa`
  ADD CONSTRAINT `fk_kelassiswa_ajar` FOREIGN KEY (`idkelasajar`) REFERENCES `kelasajar` (`idkelasajar`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kelassiswa_siswa` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pelanggaransiswa`
--
ALTER TABLE `pelanggaransiswa`
  ADD CONSTRAINT `fk_pelanggaransiswa_siswa` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pelanggaransiswa_tahun` FOREIGN KEY (`idtahun`) REFERENCES `tahunakademik` (`idtahun`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
