$(function () {
    $('.tampilModalUbah').on('click', function () {
        $('#newMenuModalLabel').html('Edit Category');
        $('.modal-footer button[type=submit]').html('Edit');
        const id = $(this).data('id');
        $('.modal-content form').attr('action', 'http://localhost:8080/web-tokoemak/produk/ubahproduk')

        $.ajax({
            url: 'http://localhost:8080/web-tokoemak/produk/getubahproduk',
            data: { id: id },
            method: 'post',
            dataType: 'json',
            success: function (data) {
                $('#code').val(data.kode_kategori);
                $('#name').val(data.nama);
                $('#image').val(data.gambar);
            }

        });


    });
    $('.tombolTambahKategori').on('click', function () {
        $('#newMenuModalLabel').html('Add new category');
        $('.modal-footer button[type=submit]').html('Add');
        const kode = $(this).data('kode');
        $('#code').val(kode);
        $('#name').val('');
        $('#image').val('');
    });
});