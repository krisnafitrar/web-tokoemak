$(function () {
    $('.editProduk').on('click', function () {
        $('#newMenuModalLabel').html('Edit Produk');
        $('.modal-footer button[type=submit]').html('Edit');
        const id = $(this).data('id');
        $('.modal-content form').attr('action', 'http://localhost:8080/web-tokoemak/produk/ubahbarang');

        $.ajax({
            url: 'http://localhost:8080/web-tokoemak/produk/getubahbarang',
            data: { id: id },
            method: 'post',
            dataType: 'json',
            success: function (data) {

                $('#code_produk').val(data.kode_barang);
                $('#code_category').val(data.kode_kategori);
                $('#name').val(data.nama);
                $('#price').val(data.harga);
                $('#stock').val(data.stok);
                $('#sold').val(data.terjual);
                $('#percentation').val(data.persentase_penjualan);
                $('#image').val(data.gambar);
            }

        });
    });
    $('.addProduk').on('click', function () {
        $('#newMenuModalLabel').html('Add New Produk');
        $('.modal-footer button[type=submit]').html('Add');
        $('.modal-content form').attr('action', 'http://localhost:8080/web-tokoemak/produk/index');
        const id = $(this).data('id');
        $('#code_produk').val(id);
        $('#code_category').val('');
        $('#name').val('');
        $('#price').val('');
        $('#stock').val('');
        $('#sold').val(0);
        $('#percentation').val(0);
        $('#image').val('');
    });
});