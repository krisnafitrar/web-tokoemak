            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
                    <a href="#" class="btn btn-primary mb-3">Create new expenditure</a>
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $this->session->flashdata('message'); ?>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Based on</th>
                                        <th scope="col">Access</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Yearly</td>
                                        <td>
                                            <a href="#" class="badge badge-primary">Edit</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Monthly</td>
                                        <td>
                                            <a href="#" class="badge badge-primary">Edit</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Daily</td>
                                        <td>
                                            <a href="#" class="badge badge-primary">Edit</a>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->