            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
                    <div class="row">
                        <div class="col-lg">
                            <?= $this->session->flashdata('message'); ?>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Month</th>
                                        <th scope="col">Year</th>
                                        <th scope="col">Income</th>
                                        <th scope="col">Expenditure</th>
                                        <th scope="col">Total Profit</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>October</td>
                                        <td>2019</td>
                                        <td>Rp.26.500.120</td>
                                        <td>Rp.15.200.050</td>
                                        <td>Rp.11.300.070</td>
                                        <td>Waiting</td>
                                        <td>
                                            <a href="#" class="badge badge-success">Confirm</a>
                                            <a href="#" class="badge badge-warning">Audit</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->