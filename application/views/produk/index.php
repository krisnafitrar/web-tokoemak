                            <?php
                            $kode;

                            if ($rows < 9) {
                                $kode = "B00";
                            } else if ($rows > 8 && $rows < 99) {
                                $kode = "B0";
                            } else {
                                $kode = "B";
                            }
                            ?>
<!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

                    <a href="" class="btn btn-primary mb-3 addProduk" data-toggle="modal" data-target="#newMenuModal" data-id="<?= $kode . ($rows + 1)?>">Add New Produk</a>

                    <div class="row">
                        <div class="col-lg-12">
                            <?php if (validation_errors()) : ?>
                                <div class="alert alert-danger" role="alert">
                                    <?= validation_errors(); ?>
                                </div>
                            <?php endif; ?>
                            
                            <?= $this->session->flashdata('message'); ?>
                            <?= $this->session->flashdata('delete'); ?>
                            <?= $this->session->flashdata('submenu'); ?>
                            <?= $this->session->flashdata('submenu_delete'); ?>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Produk Code</th>
                                        <th scope="col">Category Code</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Stock</th>
                                        <th scope="col">Sold</th>
                                        <th scope="col">Percentation</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($produk as $p) : ?>
                                        <tr>
                                            <th scope="row"><?= ++$start; ?></th>
                                            <td><?= $p['kode_barang']; ?></td>
                                            <td><?= $p['kode_kategori']; ?></td>
                                            <td><?= $p['nama']; ?></td>
                                            <td>Rp.<?= convertRupiah($p['harga']); ?></td>
                                            <td align="center"><?= $p['stok']; ?></td>
                                            <td align="center"><?= $p['terjual']; ?></td>
                                            <td align="center"><?= $p['persentase_penjualan']; ?>%</td>
                                            <td>Hidden</td>
                                            <td>
                                                <a href="#" data-toggle="modal" data-target="#newMenuModal" class="badge badge-success editProduk" data-id="<?= $p['kode_barang']; ?>">Edit</a>
                                                <a href="#" data-toggle="modal" data-target="#modal-delete" class="badge badge-danger">Delete</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    <?php
                                    function convertRupiah($uang)
                                    {
                                        return number_format($uang, 0, ".", ".");
                                    }

                                    ?>
                                </tbody>
                            </table>
                            <?= $this->pagination->create_links(); ?>
                        </div>
                    </div>


                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Modal -->
            <div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="newMenuModalLabel">Add New Produk</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="<?= base_url('produk') ?>" method="post">
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="code_produk" name="code_produk" placeholder="Produk code" value="<?= $kode . ($rows+1) ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <select name="code_category" id="code_category" class="form-control">
                                        <option value="">Select Category</option>
                                        <?php foreach ($category as $c) : ?>
                                            <option value="<?= $c['kode_kategori']; ?>"> <?= $c['nama']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Produk name">
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" id="price" name="price" placeholder="Price">
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" id="stock" name="stock" placeholder="Stock">
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" id="sold" name="sold" placeholder="Sold" value="0" readonly>
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" id="percentation" name="percentation" placeholder="Percentation" value="0" readonly>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="image" name="image" placeholder="Image url">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal" tabindex="-1" role="dialog" id="modal-delete">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Delete Produk</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Do you want to delete this produk?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <a href="<?= base_url('produk/deleteProduk') ?>?id=<?= $p['kode_barang']; ?>" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                </div>
            </div>