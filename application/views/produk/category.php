            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
                    <?php
                    $kode;

                    if (count($category) < 9) {
                        $kode = "K00";
                    } else if (count($category) > 8 && count($category) < 99) {
                        $kode = "K0";
                    } else {
                        $kode = "K";
                    }
                    ?>
                    <a href="" class="btn btn-primary mb-3 tombolTambahKategori" data-kode="<?= $kode . (count($category) + 1) ?>" data-toggle="modal" data-target="#newMenuModal">Add New Category</a>

                    <div class="row">
                        <div class="col-lg-12">
                            <?php if (validation_errors()) : ?>
                                <div class="alert alert-danger" role="alert">
                                    <?= validation_errors(); ?>
                                </div>
                            <?php endif; ?>

                            <?= $this->session->flashdata('message'); ?>
                            <?= $this->session->flashdata('delete'); ?>
                            <?= $this->session->flashdata('submenu'); ?>
                            <?= $this->session->flashdata('submenu_delete'); ?>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Category Code</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($category as $c) : ?>
                                        <tr>
                                            <th scope="row"><?= $i; ?></th>
                                            <td><?= $c['kode_kategori']; ?></td>
                                            <td><?= $c['nama']; ?></td>
                                            <td><?= $c['gambar']; ?></td>
                                            <td>
                                                <a href="#" class="badge badge-success tampilModalUbah" data-toggle="modal" data-target="#newMenuModal" data-id="<?= $c['kode_kategori']; ?>">Edit</a>
                                                <a href="#" data-toggle="modal" data-target="#modal-delete" class="badge badge-danger">Delete</a>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Modal -->
            <div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="newMenuModalLabel">Add New Category</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="<?= base_url('produk/addCategory') ?>" method="post">
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="code" name="code" placeholder="Category code" value="<?= $kode . (count($category) + 1) ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Category name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="image" name="image" placeholder="Image url">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal" tabindex="-1" role="dialog" id="modal-delete">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Delete Category</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Do you want to delete this category?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <a href="<?= base_url('produk/deleteCategory') ?>?id=<?= $c['kode_kategori']; ?>" class="btn btn-danger">Delete</a>
                        </div>
                    </div>
                </div>
            </div>