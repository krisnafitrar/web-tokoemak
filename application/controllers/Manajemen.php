<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manajemen extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('Manajemen_model', 'manage');
        $this->load->model('Auth_model', 'auth');

        $data['title'] = 'Data Siswa';
        $data['profile'] = 'Data Siswa';
        $data['user'] = $this->auth->getUser($this->session->userdata['email']);
        $data['siswa'] = $this->manage->getAllSiswa();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('manajemen/index', $data);
        $this->load->view('templates/footer');
    }
}
