<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Management extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'Produk Sales';
        $data['profile'] = 'Produk Sales';
        $data['user'] = $this->db->get_where('admin', ['email' => $this->session->userdata['email']])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('management/index', $data);
        $this->load->view('templates/footer');
    }

    public function income()
    {
        $data['title'] = 'Income';
        $data['profile'] = 'Tokoemak Income';
        $data['user'] = $this->db->get_where('admin', ['email' => $this->session->userdata['email']])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('management/income', $data);
        $this->load->view('templates/footer');
    }

    public function expenditure()
    {
        $data['title'] = 'Expenditure';
        $data['profile'] = 'Expenditure';
        $data['user'] = $this->db->get_where('admin', ['email' => $this->session->userdata['email']])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('management/expenditure', $data);
        $this->load->view('templates/footer');
    }

    public function totalprofit()
    {
        $data['title'] = 'Total Profit';
        $data['profile'] = 'Total Profit';
        $data['user'] = $this->db->get_where('admin', ['email' => $this->session->userdata['email']])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('management/totalprofit', $data);
        $this->load->view('templates/footer');
    }
}
