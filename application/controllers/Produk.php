<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('email')) {
            redirect('auth');
        }
    }

    public function index()
    {
        $data['title'] = 'Produk';
        $data['profile'] = 'Produk';
        $data['user'] = $this->db->get_where('admin', ['email' =>
        $this->session->userdata['email']])->row_array();

        //load library
        $this->load->library('pagination');

        //config
        $data['rows'] = $this->db->get('barang')->num_rows();
        $config['base_url'] = 'http://localhost:8080/web-tokoemak/produk/index';
        $config['total_rows'] = $data['rows'];
        $config['per_page'] = 10;

        //styling
        $config['full_tag_open'] = '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav>';

        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';

        $config['attributes'] = array('class' => 'page-link');


        //init
        $this->pagination->initialize($config);

        $data['start'] = $this->uri->segment(3);
        $this->db->order_by('persentase_penjualan', 'DESC');
        $data['produk'] = $this->db->get('barang', $config['per_page'], $data['start'])->result_array();
        $data['category'] = $this->db->get('kategori')->result_array();

        $this->form_validation->set_rules('name', 'Produk name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('stock', 'Stock', 'required');
        $this->form_validation->set_rules('image', 'Image', 'required');
        $this->form_validation->set_rules('code_category', 'Category code', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('produk/index', $data);
            $this->load->view('templates/footer');
        } else {
            $code_produk = $this->input->post('code_produk');
            $code_category = $this->input->post('code_category');
            $name = $this->input->post('name');
            $price = $this->input->post('price');
            $stock = $this->input->post('stock');
            $sold = $this->input->post('sold');
            $percentation = $this->input->post('percentation');
            $image = $this->input->post('image');


            $data = [
                'kode_barang' => $code_produk,
                'kode_kategori' => $code_category,
                'nama' => $name,
                'harga' => $price,
                'stok' => $stock,
                'terjual' => $sold,
                'persentase_penjualan' => $percentation,
                'gambar' => $image
            ];

            $result = $this->db->insert('barang', $data);

            if ($result) {
                $this->session->set_flashdata('submenu', '<div class="alert alert-success" role="alert">
            New produk added!
            </div>');
                redirect('produk');
            }
        }
    }

    public function categoryProduk()
    {
        $data['title'] = 'Category';
        $data['profile'] = 'Category';
        $data['user'] = $this->db->get_where('admin', ['email' =>
        $this->session->userdata['email']])->row_array();
        $data['category'] = $this->db->get('kategori')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('produk/category', $data);
        $this->load->view('templates/footer');
    }

    public function addCategory()
    {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('image', 'Image', 'required');

        $code = $this->input->post('code');
        $name = $this->input->post('name');
        $image = $this->input->post('image');

        $data = [
            'kode_kategori' => $code,
            'nama' => $name,
            'gambar' => $image
        ];
        $this->db->where('kode_kategori', $code);
        $query = $this->db->get('kategori');

        if ($query->num_rows() < 1) {
            $result = $this->db->insert('kategori', $data);
            if ($result) {
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                New category added!
                </div>');
                redirect('produk/categoryProduk');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Category code has already used!
                </div>');
            redirect('produk/categoryProduk');
        }
    }

    public function deleteCategory()
    {
        $id = $_GET['id'];
        $query = "DELETE FROM kategori WHERE kode_kategori='$id' ";

        $result = $this->db->query($query);

        if ($result) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Category deleted!
            </div>');
            redirect('produk/categoryProduk');
        }
    }

    public function getUbahProduk()
    {
        $id = $_POST['id'];
        $query = $this->db->get_where('kategori', ['kode_kategori' => $id])->row_array();
        echo json_encode($query);
    }

    public function ubahProduk()
    {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('image', 'Image', 'required');

        $code = $this->input->post('code');
        $name = $this->input->post('name');
        $image = $this->input->post('image');

        $data = [
            'kode_kategori' => $code,
            'nama' => $name,
            'gambar' => $image
        ];

        $this->db->set($data);
        $this->db->where('kode_kategori', $code);
        $result = $this->db->update('kategori');
        if ($result) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Category updated!
                </div>');
            redirect('produk/categoryProduk');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Failed!
                </div>');
            redirect('produk/categoryProduk');
        }
    }

    public function deleteProduk()
    {
        $id = $_GET['id'];

        $this->db->where('kode_barang', $id);
        $result = $this->db->delete('barang');

        if ($result) {
            $this->session->set_flashdata('submenu', '<div class="alert alert-success" role="alert">
            Produk deleted!
            </div>');
            redirect('produk');
        }
    }

    public function ubahBarang()
    {
        $this->form_validation->set_rules('name', 'Produk name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('stock', 'Stock', 'required');
        $this->form_validation->set_rules('image', 'Image', 'required');
        $this->form_validation->set_rules('code_category', 'Category code', 'required');

        $code_produk = $this->input->post('code_produk');
        $code_category = $this->input->post('code_category');
        $name = $this->input->post('name');
        $price = $this->input->post('price');
        $stock = $this->input->post('stock');
        $sold = $this->input->post('sold');
        $percentation = $this->input->post('percentation');
        $image = $this->input->post('image');


        $data = [
            'kode_barang' => $code_produk,
            'kode_kategori' => $code_category,
            'nama' => $name,
            'harga' => $price,
            'stok' => $stock,
            'terjual' => $sold,
            'persentase_penjualan' => $percentation,
            'gambar' => $image
        ];

        $this->db->where('kode_barang', $code_produk);
        $result = $this->db->update('barang', $data);

        if ($result) {
            $this->session->set_flashdata('submenu', '<div class="alert alert-success" role="alert">
            Produk updated!
            </div>');
            redirect('produk');
        }
    }

    public function getUbahBarang()
    {
        $id = $_POST['id'];
        $query = $this->db->get_where('barang', ['kode_barang' => $id])->row_array();
        echo json_encode($query);
    }

    public function filter()
    {
        $this->form_validation->set_rules('name', 'Filter Name', 'required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Filter';
            $data['profile'] = 'Filter';
            $data['user'] = $this->db->get_where('admin', ['email' =>
            $this->session->userdata['email']])->row_array();
            $data['filter'] = $this->db->get('filter_barang')->result_array();
            $data['category'] = $this->db->get('kategori')->result_array();

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('produk/filter', $data);
            $this->load->view('templates/footer');
        } else {
            $name = $this->input->post('name');
            $category_code = $this->input->post('code_category');
            $data = [
                'id' => '',
                'kode_kategori' => $category_code,
                'nama' => $name
            ];

            $this->db->insert('filter_barang', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            New Filter Added!
            </div>');
            redirect('produk/filter');
        }
    }

    public function deleteFilter()
    {
        $id = $_GET['id'];

        $this->db->where('id', $id);
        $this->db->delete('filter_barang');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Filter Deleted!
            </div>');
        redirect('produk/filter');
    }
}
