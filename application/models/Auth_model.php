<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{

    public function userLogin($params1, $params2)
    {
        $email = $params1;
        $password = $params2;

        $user = $this->db->get_where('users', ['email' => $email])->row_array();

        if ($user) {
            if ($user['is_active'] == 1) {
                if (password_verify($password, $user['password'])) {
                    return $user;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function getUser($data)
    {
        return $this->db->get_where('users', ['email' => $data])->row_array();
    }


    public function setUser($data)
    {
        $this->db->insert('users', $data);
    }
}
